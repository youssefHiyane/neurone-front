import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-employee-component',
  templateUrl: './employee-component.component.html',
  styleUrls: ['./employee-component.component.scss']
})
export class EmployeeComponentComponent implements OnInit {
  listeEmployee:any;
  constructor(private httpClient:HttpClient) { }

  ngOnInit(): void {
    this.httpClient.get('http://localhost:9090/employees')
      .subscribe(data => {
        this.listeEmployee = data;
      },err => {
        console.log(err);
      });
  }

}
