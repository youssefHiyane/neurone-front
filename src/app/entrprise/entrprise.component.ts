import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-entrprise',
  templateUrl: './entrprise.component.html',
  styleUrls: ['./entrprise.component.scss']
})
export class EntrpriseComponent implements OnInit {
  listeEntreprises:any;
  constructor(private httpClient:HttpClient) { }

  ngOnInit(): void {

    this.httpClient.get('http://localhost:9090/entreprises')
      .subscribe(data => {
        this.listeEntreprises = data;
      },err => {
        console.log(err);
      });
  }

}
