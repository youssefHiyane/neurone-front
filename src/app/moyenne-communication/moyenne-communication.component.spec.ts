import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoyenneCommunicationComponent } from './moyenne-communication.component';

describe('MoyenneCommunicationComponent', () => {
  let component: MoyenneCommunicationComponent;
  let fixture: ComponentFixture<MoyenneCommunicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoyenneCommunicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoyenneCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
