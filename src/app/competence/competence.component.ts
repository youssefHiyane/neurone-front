import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-competence',
  templateUrl: './competence.component.html',
  styleUrls: ['./competence.component.scss']
})
export class CompetenceComponent implements OnInit {
  listCompetences:any;
  constructor(private httpClient:HttpClient) { }

  ngOnInit(): void {

    this.httpClient.get('http://localhost:9090/competences')
      .subscribe(data => {
        this.listCompetences = data;
      },err => {
        console.log(err);
      });
  }

}
