import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NiveauEvaluationComponent } from './niveau-evaluation.component';

describe('NiveauEvaluationComponent', () => {
  let component: NiveauEvaluationComponent;
  let fixture: ComponentFixture<NiveauEvaluationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NiveauEvaluationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NiveauEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
